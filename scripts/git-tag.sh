#!/bin/sh

CURRENT_VERSION=$(grep -E "^version\s?=" Cargo.toml | cut --delimiter='"' --fields=2)
TAG="v$CURRENT_VERSION"

cargo fmt

if ! cargo test; then
	exit $?
fi

if cargo publish --dry-run; then
	echo "Creating tag: $TAG"
	echo "Do not forget to push it: git push origin $TAG"

	git tag -a "$TAG" -m "Version $CURRENT_VERSION"
else
	exit $?
fi
