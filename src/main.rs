mod lib;

use crate::lib::*;

use clap::Parser;
use std::io;

fn main() {
    let opt = Opt::parse();

    transcode(opt, &mut io::stdin(), &mut io::stdout());
}
