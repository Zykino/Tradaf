# Description
This tool "traduit" (french for translate) data formats. It was made in the hope to bring serde to the cli (serdi ^^). But that not exaclty what serde is about and I have no affiliation with them so I did not want to namesquat.

# Installation
First install [the rust toolchain](https://www.rust-lang.org/tools/install) and then execute the following command:
```sh
cargo install tradaf
```

# Usage example
Prettify (or minify) some data format for ease of reading (resp: compact and have less data to save/transfert)
```sh
tradaf json json --pretty < simple.json 
```

Litterally transcode between data formats
```sh
tradaf ron yaml < examples/standard.ron
```

Transcode into JSON so we can use `jq`, do some transformations and then transcode again into another format for viewing
```sh
tradaf RON json < examples/gameConfig.ron | jq .key_bindings | tradaf Json yaml
```

# TODOS
* [ ] Implement all [data formats](https://serde.rs/#data-formats), or at least the one that expose a serde serializer or deserializer.
  * [-] Bencode
    * [X] Deserialization
    * [ ] Serialization
  * [ ] BSON
  * [ ] CBOR
  * [-] DBus (not sure it is working, but it compile…)
  * [ ] DynamoDB
  * [ ] Envy (deserialization only)
  * [ ] Envy store (deserialization only)
  * [ ] FlexBuffer
    * The `Serializer` and `Deserializer` exposed does not work with serde-transcode.
  * [-] GVariant => See DBus: the same crate propose both
  * [ ] HJSON
  * [X] JSON
  * [-] JSON5
    * [X] Deserialization
    * [ ] Serialization
  * [ ] https://github.com/Lucretiel/kaydle
  * [ ] S-Expressions (lisp)
  * [X] Pickle
    * [ ] Test properly both way
  * [-] Query String (URL)
    * [ ] Deserialization
    * [X] Serialization
  * [-] MessagePack
    * [ ] Deserialization
    * [X] Serialization
  * [X] RON
  * [ ] TOML
  * [X] YAML
  * [ ] …
* [ ] Add tests for each serializer and each deserializer (at least one)
* [ ] Check if crates have features that we should enable/disable
  * [ ] `zvariant` looks to include async runtime by default
* [ ] Check my notes for each data format and open an issue on the crates to point problem/present my project and ask for help
* [ ] Add proper error handling (find a mentor to explain me propper error management: struct, enum, … and then use anyhow/this_error if needed)
