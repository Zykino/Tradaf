use tradaf::*;

fn example_to_vec(file: &str) -> Vec<u8> {
    use std::fs::File;
    use std::io::prelude::*;

    let mut f = File::open(file).unwrap();
    let mut buffer = vec![];
    f.read_to_end(&mut buffer).unwrap();
    buffer
}

fn example_to_string(file: &str) -> String {
    let buffer = example_to_vec(file);
    String::from_utf8(buffer).unwrap()
}

#[test]
fn ron_to_json() {
    let opt = Opt {
        input: Input::Ron,
        output: Output::Json,

        format_in: None,
        format_out: None,

        pretty: true,
        no_newline: false,
    };

    let input = example_to_vec("test-data/materials.ron");

    let mut output = vec![];
    transcode(opt, &mut input.as_slice(), &mut output);
    let out = String::from_utf8(output).unwrap();

    let control = example_to_string("test-data/materials.json");

    assert_eq!(control, out);
}

#[test]
fn json5_read_json() {
    let opt = Opt {
        input: Input::Json5,
        output: Output::Json,

        format_in: None,
        format_out: None,

        pretty: true,
        no_newline: false,
    };

    let input = example_to_vec("test-data/materials.json");

    let mut output = vec![];
    transcode(opt, &mut input.as_slice(), &mut output);
    let out = String::from_utf8(output).unwrap();

    let control = example_to_string("test-data/materials.json");

    assert_eq!(control, out);
}
